# Foundry Virtual Tabletop - Genefunk 2090 Game System

A fork of the dnd5e [Foundry Virtual Tabletop](http://foundryvtt.com) game system for the Genefunk 2090 system.

**This system is not designed as a standalone system.** It is intended to serve as a base framework for the
[Genefunk 2090 Obsidian Character Sheets](https://bitbucket.org/finalfrog/obsidiangenefunk) module.
Please ensure that you install that module in addition to this system and enable it in your campaign to get
the intended experience.

This system is offered and may be used under the terms of the Open Gaming License v1.0a and its accompanying
[Systems Reference Document 5.1 (SRD5)](http://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf).

This system provides character sheet support for Actors and Items, mechanical support for dice and rules necessary to
play games of Genefunk 2090.

The software component of this system is distributed under the GNUv3 license.

## Installation Instructions

To install and use the Genefunk system for Foundry Virtual Tabletop, simply paste the following URL into the 
**Install System** dialog on the Setup menu of the application.

https://gitlab.com/finalfrog/genefunk/raw/genefunkMain/system.json

If you wish to manually install the system, you must clone or extract it into the ``Data/systems/genefunk`` folder. You
may do this by cloning the repository or downloading a zip archive from the
[Releases Page](https://gitlab.com/finalfrog/genefunk/-/releases).

## Community Contribution

Code and content contributions are accepted. Please feel free to submit issues to the issue tracker or submit merge
requests for code changes. Approval for such requests involves code and (if necessary) design review by Atropos. Please
reach out on the Foundry Community Discord with any questions.