import {ClassFeatures} from "./classFeatures.js"

// Namespace Configuration Values
export const GENEFUNK = {};

// ASCII Artwork
GENEFUNK.ASCII = `_______________________________
______      ______ _____ _____
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__
| | | / _ \\/\\ | | |   \\ \\  __|
| |/ / (_>  < |/ //\\__/ / |___
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;


/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
GENEFUNK.abilities = {
  "str": "GENEFUNK.AbilityStr",
  "dex": "GENEFUNK.AbilityDex",
  "con": "GENEFUNK.AbilityCon",
  "int": "GENEFUNK.AbilityInt",
  "wis": "GENEFUNK.AbilityWis",
  "cha": "GENEFUNK.AbilityCha"
};

GENEFUNK.abilityAbbreviations = {
  "str": "GENEFUNK.AbilityStrAbbr",
  "dex": "GENEFUNK.AbilityDexAbbr",
  "con": "GENEFUNK.AbilityConAbbr",
  "int": "GENEFUNK.AbilityIntAbbr",
  "wis": "GENEFUNK.AbilityWisAbbr",
  "cha": "GENEFUNK.AbilityChaAbbr"
};

/* -------------------------------------------- */

/**
 * Character alignment options
 * @type {Object}
 */
GENEFUNK.alignments = {
  'lg': "GENEFUNK.AlignmentLG",
  'ng': "GENEFUNK.AlignmentNG",
  'cg': "GENEFUNK.AlignmentCG",
  'ln': "GENEFUNK.AlignmentLN",
  'tn': "GENEFUNK.AlignmentTN",
  'cn': "GENEFUNK.AlignmentCN",
  'le': "GENEFUNK.AlignmentLE",
  'ne': "GENEFUNK.AlignmentNE",
  'ce': "GENEFUNK.AlignmentCE"
};

/* -------------------------------------------- */

/**
 * An enumeration of item attunement states
 * @type {{"0": string, "1": string, "2": string}}
 */
GENEFUNK.attunements = {
  0: "GENEFUNK.AttunementNone",
  1: "GENEFUNK.AttunementRequired",
  2: "GENEFUNK.AttunementAttuned"
};

/* -------------------------------------------- */


GENEFUNK.weaponProficiencies = {
  "sim": "GENEFUNK.WeaponSimpleProficiency",
  "mar": "GENEFUNK.WeaponMartialProficiency"
};

GENEFUNK.toolProficiencies = {
  "art": "GENEFUNK.ToolArtisans",
  "disg": "GENEFUNK.ToolDisguiseKit",
  "forg": "GENEFUNK.ToolForgeryKit",
  "game": "GENEFUNK.ToolGamingSet",
  "herb": "GENEFUNK.ToolHerbalismKit",
  "music": "GENEFUNK.ToolMusicalInstrument",
  "navg": "GENEFUNK.ToolNavigators",
  "pois": "GENEFUNK.ToolPoisonersKit",
  "thief": "GENEFUNK.ToolThieves",
  "vehicle": "GENEFUNK.ToolVehicle"
};


/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
GENEFUNK.timePeriods = {
  "inst": "GENEFUNK.TimeInst",
  "turn": "GENEFUNK.TimeTurn",
  "round": "GENEFUNK.TimeRound",
  "minute": "GENEFUNK.TimeMinute",
  "hour": "GENEFUNK.TimeHour",
  "day": "GENEFUNK.TimeDay",
  "month": "GENEFUNK.TimeMonth",
  "year": "GENEFUNK.TimeYear",
  "perm": "GENEFUNK.TimePerm",
  "spec": "GENEFUNK.Special"
};


/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
GENEFUNK.abilityActivationTypes = {
  "none": "GENEFUNK.None",
  "action": "GENEFUNK.Action",
  "bonus": "GENEFUNK.BonusAction",
  "reaction": "GENEFUNK.Reaction",
  "minute": GENEFUNK.timePeriods.minute,
  "hour": GENEFUNK.timePeriods.hour,
  "day": GENEFUNK.timePeriods.day,
  "special": GENEFUNK.timePeriods.spec,
  "legendary": "GENEFUNK.LegAct",
  "lair": "GENEFUNK.LairAct",
  "crew": "GENEFUNK.VehicleCrewAction"
};

/* -------------------------------------------- */


GENEFUNK.abilityConsumptionTypes = {
  "ammo": "GENEFUNK.ConsumeAmmunition",
  "attribute": "GENEFUNK.ConsumeAttribute",
  "material": "GENEFUNK.ConsumeMaterial",
  "charges": "GENEFUNK.ConsumeCharges"
};


/* -------------------------------------------- */

// Creature Sizes
GENEFUNK.actorSizes = {
  "tiny": "GENEFUNK.SizeTiny",
  "sm": "GENEFUNK.SizeSmall",
  "med": "GENEFUNK.SizeMedium",
  "lg": "GENEFUNK.SizeLarge",
  "huge": "GENEFUNK.SizeHuge",
  "grg": "GENEFUNK.SizeGargantuan"
};

GENEFUNK.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
GENEFUNK.itemActionTypes = {
  "mwak": "GENEFUNK.ActionMWAK",
  "rwak": "GENEFUNK.ActionRWAK",
  "msak": "GENEFUNK.ActionMSAK",
  "rsak": "GENEFUNK.ActionRSAK",
  "save": "GENEFUNK.ActionSave",
  "heal": "GENEFUNK.ActionHeal",
  "abil": "GENEFUNK.ActionAbil",
  "util": "GENEFUNK.ActionUtil",
  "other": "GENEFUNK.ActionOther"
};

/* -------------------------------------------- */

GENEFUNK.itemCapacityTypes = {
  "items": "GENEFUNK.ItemContainerCapacityItems",
  "weight": "GENEFUNK.ItemContainerCapacityWeight"
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
GENEFUNK.limitedUsePeriods = {
  "sr": "GENEFUNK.ShortRest",
  "lr": "GENEFUNK.LongRest",
  "day": "GENEFUNK.Day",
  "charges": "GENEFUNK.Charges"
};


/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
GENEFUNK.equipmentTypes = {
  "light": "GENEFUNK.EquipmentLight",
  "medium": "GENEFUNK.EquipmentMedium",
  "heavy": "GENEFUNK.EquipmentHeavy",
  "bonus": "GENEFUNK.EquipmentBonus",
  "natural": "GENEFUNK.EquipmentNatural",
  "shield": "GENEFUNK.EquipmentShield",
  "clothing": "GENEFUNK.EquipmentClothing",
  "trinket": "GENEFUNK.EquipmentTrinket",
  "vehicle": "GENEFUNK.EquipmentVehicle"
};


/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
GENEFUNK.armorProficiencies = {
  "lgt": GENEFUNK.equipmentTypes.light,
  "med": GENEFUNK.equipmentTypes.medium,
  "hvy": GENEFUNK.equipmentTypes.heavy,
  "shl": "GENEFUNK.EquipmentShieldProficiency"
};


/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
GENEFUNK.consumableTypes = {
  "ammo": "GENEFUNK.ConsumableAmmunition",
  "potion": "GENEFUNK.ConsumablePotion",
  "poison": "GENEFUNK.ConsumablePoison",
  "food": "GENEFUNK.ConsumableFood",
  "scroll": "GENEFUNK.ConsumableScroll",
  "wand": "GENEFUNK.ConsumableWand",
  "rod": "GENEFUNK.ConsumableRod",
  "trinket": "GENEFUNK.ConsumableTrinket"
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the 5e system
 * @type {Object}
 */
GENEFUNK.currencies = {
  "gp": "GENEFUNK.CurrencyGP",
  "sp": "GENEFUNK.CurrencySP",
  "cp": "GENEFUNK.CurrencyCP",
};


/**
 * Define the upwards-conversion rules for registered currency types
 * @type {{string, object}}
 */
GENEFUNK.currencyConversion = {
  cp: {into: "sp", each: 100},
  sp: {into: "gp", each: 1000000 }
};

/* -------------------------------------------- */


// Damage Types
GENEFUNK.damageTypes = {
  "acid": "GENEFUNK.DamageAcid",
  "bludgeoning": "GENEFUNK.DamageBludgeoning",
  "cold": "GENEFUNK.DamageCold",
  "fire": "GENEFUNK.DamageFire",
  "force": "GENEFUNK.DamageForce",
  "lightning": "GENEFUNK.DamageLightning",
  "necrotic": "GENEFUNK.DamageNecrotic",
  "piercing": "GENEFUNK.DamagePiercing",
  "poison": "GENEFUNK.DamagePoison",
  "psychic": "GENEFUNK.DamagePsychic",
  "radiant": "GENEFUNK.DamageRadiant",
  "slashing": "GENEFUNK.DamageSlashing",
  "thunder": "GENEFUNK.DamageThunder"
};

// Damage Resistance Types
GENEFUNK.damageResistanceTypes = mergeObject(duplicate(GENEFUNK.damageTypes), {
  "physical": "GENEFUNK.DamagePhysical"
});


/* -------------------------------------------- */

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
GENEFUNK.movementTypes = {
  "burrow": "GENEFUNK.MovementBurrow",
  "climb": "GENEFUNK.MovementClimb",
  "fly": "GENEFUNK.MovementFly",
  "swim": "GENEFUNK.MovementSwim",
  "walk": "GENEFUNK.MovementWalk",
}

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
GENEFUNK.movementUnits = {
  "ft": "GENEFUNK.DistFt",
  "mi": "GENEFUNK.DistMi"
}

/**
 * The valid units of measure for the range of an action or effect.
 * This object automatically includes the movement units from GENEFUNK.movementUnits
 * @type {Object<string,string>}
 */
GENEFUNK.distanceUnits = {
  "none": "GENEFUNK.None",
  "self": "GENEFUNK.DistSelf",
  "touch": "GENEFUNK.DistTouch",
  "spec": "GENEFUNK.Special",
  "any": "GENEFUNK.DistAny"
};
for ( let [k, v] of Object.entries(GENEFUNK.movementUnits) ) {
  GENEFUNK.distanceUnits[k] = v;
}

/* -------------------------------------------- */


/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
GENEFUNK.encumbrance = {
  currencyPerWeight: 0,
  strMultiplier: 7,
  vehicleWeightMultiplier: 1 // 2000 lbs in a ton
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied
 * @type {Object}
 */
GENEFUNK.targetTypes = {
  "none": "GENEFUNK.None",
  "self": "GENEFUNK.TargetSelf",
  "creature": "GENEFUNK.TargetCreature",
  "ally": "GENEFUNK.TargetAlly",
  "enemy": "GENEFUNK.TargetEnemy",
  "object": "GENEFUNK.TargetObject",
  "space": "GENEFUNK.TargetSpace",
  "radius": "GENEFUNK.TargetRadius",
  "sphere": "GENEFUNK.TargetSphere",
  "cylinder": "GENEFUNK.TargetCylinder",
  "cone": "GENEFUNK.TargetCone",
  "square": "GENEFUNK.TargetSquare",
  "cube": "GENEFUNK.TargetCube",
  "line": "GENEFUNK.TargetLine",
  "wall": "GENEFUNK.TargetWall"
};


/* -------------------------------------------- */


/**
 * Map the subset of target types which produce a template area of effect
 * The keys are GENEFUNK target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
GENEFUNK.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};


/* -------------------------------------------- */

// Healing Types
GENEFUNK.healingTypes = {
  "healing": "GENEFUNK.Healing",
  "temphp": "GENEFUNK.HealingTemp"
};


/* -------------------------------------------- */


/**
 * Enumerate the denominations of hit dice which can apply to classes
 * @type {Array.<string>}
 */
GENEFUNK.hitDieTypes = ["d6", "d8", "d10", "d12"];


/* -------------------------------------------- */

/**
 * The set of possible sensory perception types which an Actor may have
 * @type {object}
 */
GENEFUNK.senses = {
  "blindsight": "GENEFUNK.SenseBlindsight",
  "darkvision": "GENEFUNK.SenseDarkvision",
  "acuteolfaction": "GENEFUNK.SenseAcuteOlfaction",
  "macrovision": "GENEFUNK.SenseMacrovision",
  "microvision": "GENEFUNK.SenseMicrovision",
  "penetrationvision": "GENEFUNK.SensePenetrationVision",
  "spectrumvision": "GENEFUNK.SenseSpectrumVision"
};

/* -------------------------------------------- */

/**
 * The set of skill which can be trained
 * @type {Object}
 */
GENEFUNK.skills = {
  "acr": "GENEFUNK.SkillAcr",
  "ath": "GENEFUNK.SkillAth",
  "bur": "GENEFUNK.SkillBur",
  "com": "GENEFUNK.SkillCom",
  "dec": "GENEFUNK.SkillDec",
  "dri": "GENEFUNK.SkillDri",
  "ins": "GENEFUNK.SkillIns",
  "itm": "GENEFUNK.SkillItm",
  "inv": "GENEFUNK.SkillInv",
  "lif": "GENEFUNK.SkillLif",
  "mec": "GENEFUNK.SkillMec",
  "prc": "GENEFUNK.SkillPrc",
  "prf": "GENEFUNK.SkillPrf",
  "per": "GENEFUNK.SkillPer",
  "phy": "GENEFUNK.SkillPhy",
  "slt": "GENEFUNK.SkillSlt",
  "soc": "GENEFUNK.SkillSoc",
  "ste": "GENEFUNK.SkillSte",
  "str": "GENEFUNK.SkillStr",
  "sur": "GENEFUNK.SkillSur"
};


/* -------------------------------------------- */

GENEFUNK.spellPreparationModes = {
  "prepared": "GENEFUNK.SpellPrepPrepared",
  "pact": "GENEFUNK.PactMagic",
  "always": "GENEFUNK.SpellPrepAlways",
  "atwill": "GENEFUNK.SpellPrepAtWill",
  "innate": "GENEFUNK.SpellPrepInnate"
};

GENEFUNK.spellUpcastModes = ["always", "pact", "prepared"];

GENEFUNK.spellProgression = {
  "none": "GENEFUNK.SpellNone",
  "full": "GENEFUNK.SpellProgFull",
  "half": "GENEFUNK.SpellProgHalf",
  "third": "GENEFUNK.SpellProgThird",
  "pact": "GENEFUNK.SpellProgPact",
  "artificer": "GENEFUNK.SpellProgArt"
};

/* -------------------------------------------- */

/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
GENEFUNK.spellScalingModes = {
  "none": "GENEFUNK.SpellNone",
  "cantrip": "GENEFUNK.SpellCantrip",
  "level": "GENEFUNK.SpellLevel"
};

/* -------------------------------------------- */


/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
GENEFUNK.weaponTypes = {
  "simpleM": "GENEFUNK.WeaponSimpleM",
  "simpleR": "GENEFUNK.WeaponSimpleR",
  "martialM": "GENEFUNK.WeaponMartialM",
  "martialR": "GENEFUNK.WeaponMartialR",
  "natural": "GENEFUNK.WeaponNatural",
  "improv": "GENEFUNK.WeaponImprov",
  "siege": "GENEFUNK.WeaponSiege"
};


/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
GENEFUNK.weaponProperties = {
  "amm": "GENEFUNK.WeaponPropertiesAmm",
  "hvy": "GENEFUNK.WeaponPropertiesHvy",
  "fin": "GENEFUNK.WeaponPropertiesFin",
  "fir": "GENEFUNK.WeaponPropertiesFir",
  "foc": "GENEFUNK.WeaponPropertiesFoc",
  "lgt": "GENEFUNK.WeaponPropertiesLgt",
  "lod": "GENEFUNK.WeaponPropertiesLod",
  "rch": "GENEFUNK.WeaponPropertiesRch",
  "rel": "GENEFUNK.WeaponPropertiesRel",
  "ret": "GENEFUNK.WeaponPropertiesRet",
  "spc": "GENEFUNK.WeaponPropertiesSpc",
  "thr": "GENEFUNK.WeaponPropertiesThr",
  "two": "GENEFUNK.WeaponPropertiesTwo",
  "ver": "GENEFUNK.WeaponPropertiesVer"
};


// Spell Components
GENEFUNK.spellComponents = {
  "V": "GENEFUNK.ComponentVerbal",
  "S": "GENEFUNK.ComponentSomatic",
  "M": "GENEFUNK.ComponentMaterial"
};

// Spell Schools
GENEFUNK.spellSchools = {
  "inj": "GENEFUNK.SchoolInj",
  "gag": "GENEFUNK.SchoolGad",
  "cte": "GENEFUNK.SchoolCte",
  "ctb": "GENEFUNK.SchoolCtb",
  "cex": "GENEFUNK.SchoolCex",
  "bot": "GENEFUNK.SchoolBot",
  "min": "GENEFUNK.SchoolMin",
  "sof": "GENEFUNK.SchoolSof",
  "mis": "GENEFUNK.SchoolMis",
  "mib": "GENEFUNK.SchoolMib"
};

// Spell Levels
GENEFUNK.spellLevels = {
  0: "GENEFUNK.SpellLevel0",
  1: "GENEFUNK.SpellLevel1",
  2: "GENEFUNK.SpellLevel2",
  3: "GENEFUNK.SpellLevel3",
  4: "GENEFUNK.SpellLevel4",
  5: "GENEFUNK.SpellLevel5",
  6: "GENEFUNK.SpellLevel6",
  7: "GENEFUNK.SpellLevel7",
  8: "GENEFUNK.SpellLevel8",
  9: "GENEFUNK.SpellLevel9"
};

// Spell Scroll Compendium UUIDs
GENEFUNK.spellScrollIds = {
  0: 'Compendium.genefunk.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.genefunk.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.genefunk.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.genefunk.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.genefunk.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.genefunk.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.genefunk.items.tI3rWx4bxefNCexS',
  7: 'Compendium.genefunk.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.genefunk.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.genefunk.items.O4YbkJkLlnsgUszZ'
};

/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
GENEFUNK.SPELL_SLOT_TABLE = [
  [4],
  [5],
  [6],
  [7],
  [7, 1],
  [7, 2],
  [7, 3],
  [7, 4],
  [8, 5, 1],
  [8, 5, 2],
  [8, 5, 3],
  [8, 5, 4,],
  [9, 5, 5, 1],
  [9, 5, 5, 2],
  [9, 5, 5, 3],
  [9, 5, 5, 4],
  [10, 5, 5, 5, 1],
  [10, 5, 5, 5, 2],
  [10, 5, 5, 5, 3],
  [10, 5, 5, 5, 4]
];

/* -------------------------------------------- */

// Polymorph options.
GENEFUNK.polymorphSettings = {
  keepPhysical: 'GENEFUNK.PolymorphKeepPhysical',
  keepMental: 'GENEFUNK.PolymorphKeepMental',
  keepSaves: 'GENEFUNK.PolymorphKeepSaves',
  keepSkills: 'GENEFUNK.PolymorphKeepSkills',
  mergeSaves: 'GENEFUNK.PolymorphMergeSaves',
  mergeSkills: 'GENEFUNK.PolymorphMergeSkills',
  keepClass: 'GENEFUNK.PolymorphKeepClass',
  keepFeats: 'GENEFUNK.PolymorphKeepFeats',
  keepSpells: 'GENEFUNK.PolymorphKeepSpells',
  keepItems: 'GENEFUNK.PolymorphKeepItems',
  keepBio: 'GENEFUNK.PolymorphKeepBio',
  keepVision: 'GENEFUNK.PolymorphKeepVision'
};

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
GENEFUNK.proficiencyLevels = {
  0: "GENEFUNK.NotProficient",
  1: "GENEFUNK.Proficient",
  0.5: "GENEFUNK.HalfProficient",
  2: "GENEFUNK.Expertise"
};

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object.
 * In cases where multiple pieces of cover are
 * in play, we take the highest value.
 */
GENEFUNK.cover = {
  0: 'GENEFUNK.None',
  .5: 'GENEFUNK.CoverHalf',
  .75: 'GENEFUNK.CoverThreeQuarters',
  1: 'GENEFUNK.CoverTotal'
};

/* -------------------------------------------- */


// Condition Types
GENEFUNK.conditionTypes = {
  "blinded": "GENEFUNK.ConBlinded",
  "charmed": "GENEFUNK.ConCharmed",
  "deafened": "GENEFUNK.ConDeafened",
  "diseased": "GENEFUNK.ConDiseased",
  "exhaustion": "GENEFUNK.ConExhaustion",
  "frightened": "GENEFUNK.ConFrightened",
  "grappled": "GENEFUNK.ConGrappled",
  "incapacitated": "GENEFUNK.ConIncapacitated",
  "invisible": "GENEFUNK.ConInvisible",
  "paralyzed": "GENEFUNK.ConParalyzed",
  "petrified": "GENEFUNK.ConPetrified",
  "poisoned": "GENEFUNK.ConPoisoned",
  "prone": "GENEFUNK.ConProne",
  "restrained": "GENEFUNK.ConRestrained",
  "stunned": "GENEFUNK.ConStunned",
  "unconscious": "GENEFUNK.ConUnconscious"
};

// Languages
GENEFUNK.languages = {
  "common": "GENEFUNK.LanguagesCommon",
  "aarakocra": "GENEFUNK.LanguagesAarakocra",
  "abyssal": "GENEFUNK.LanguagesAbyssal",
  "aquan": "GENEFUNK.LanguagesAquan",
  "auran": "GENEFUNK.LanguagesAuran",
  "celestial": "GENEFUNK.LanguagesCelestial",
  "deep": "GENEFUNK.LanguagesDeepSpeech",
  "draconic": "GENEFUNK.LanguagesDraconic",
  "druidic": "GENEFUNK.LanguagesDruidic",
  "dwarvish": "GENEFUNK.LanguagesDwarvish",
  "elvish": "GENEFUNK.LanguagesElvish",
  "giant": "GENEFUNK.LanguagesGiant",
  "gith": "GENEFUNK.LanguagesGith",
  "gnomish": "GENEFUNK.LanguagesGnomish",
  "goblin": "GENEFUNK.LanguagesGoblin",
  "gnoll": "GENEFUNK.LanguagesGnoll",
  "halfling": "GENEFUNK.LanguagesHalfling",
  "ignan": "GENEFUNK.LanguagesIgnan",
  "infernal": "GENEFUNK.LanguagesInfernal",
  "orc": "GENEFUNK.LanguagesOrc",
  "primordial": "GENEFUNK.LanguagesPrimordial",
  "sylvan": "GENEFUNK.LanguagesSylvan",
  "terran": "GENEFUNK.LanguagesTerran",
  "cant": "GENEFUNK.LanguagesThievesCant",
  "undercommon": "GENEFUNK.LanguagesUndercommon"
};

// Character Level XP Requirements
GENEFUNK.CHARACTER_EXP_LEVELS =  [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000]
;

// Challenge Rating XP Levels
GENEFUNK.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];

// Character Features Per Class And Level
GENEFUNK.classFeatures = ClassFeatures;

// Configure Optional Character Flags
GENEFUNK.characterFlags = {
  "diamondSoul": {
    name: "GENEFUNK.FlagsDiamondSoul",
    hint: "GENEFUNK.FlagsDiamondSoulHint",
    section: "Feats",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "GENEFUNK.FlagsElvenAccuracy",
    hint: "GENEFUNK.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "GENEFUNK.FlagsHalflingLucky",
    hint: "GENEFUNK.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "GENEFUNK.FlagsInitiativeAdv",
    hint: "GENEFUNK.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "GENEFUNK.FlagsAlert",
    hint: "GENEFUNK.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "GENEFUNK.FlagsJOAT",
    hint: "GENEFUNK.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "GENEFUNK.FlagsObservant",
    hint: "GENEFUNK.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "powerfulBuild": {
    name: "GENEFUNK.FlagsPowerfulBuild",
    hint: "GENEFUNK.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "reliableTalent": {
    name: "GENEFUNK.FlagsReliableTalent",
    hint: "GENEFUNK.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "GENEFUNK.FlagsRemarkableAthlete",
    hint: "GENEFUNK.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "GENEFUNK.FlagsWeaponCritThreshold",
    hint: "GENEFUNK.FlagsWeaponCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "spellCriticalThreshold": {
    name: "GENEFUNK.FlagsSpellCritThreshold",
    hint: "GENEFUNK.FlagsSpellCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "meleeCriticalDamageDice": {
    name: "GENEFUNK.FlagsMeleeCriticalDice",
    hint: "GENEFUNK.FlagsMeleeCriticalDiceHint",
    section: "Feats",
    type: Number,
    placeholder: 0
  },
};

// Configure allowed status flags
GENEFUNK.allowedActorFlags = ["isPolymorphed", "originalActor"].concat(Object.keys(GENEFUNK.characterFlags));
